Microservices Architecture Q&A - AYMAN CHERGUI M1

- Question 1:

Microservices Architecture:
Microservices architecture is an approach where an application is built as a collection of small, independent services. Each service focuses on a specific business function and communicates with others via APIs. They can be developed, deployed, and scaled independently.

Monolithic Application:
Monolithic architecture is traditional, where all components are tightly integrated into a single codebase and deployed as one unit. The entire application, with all its functionalities, runs as a single process.


*************************

- Question 2:

Microservices:
Pros - Scalability, Flexibility, Fault Isolation, Continuous Deployment
Cons - Complexity, Distributed System Challenges, Latency

Monolithic:
Pros - Simplicity, Singular Technology Stack, Easier Debugging
Cons - Scalability, Dependency, Limited Flexibility

*************************

- Question 3:

In microservices, the application is split based on business capabilities or domains. Each microservice handles a specific business function, encapsulating its data model and logic. This enables independent development, deployment, and scaling.

*************************

- Question 4:

CAP Theorem:
- Consistency: All nodes see the same data.
- Availability: Every request gets a response.
- Partition Tolerance: The system remains operational despite network issues.

In microservices, the CAP theorem influences architects to make trade-offs, prioritizing partition tolerance and availability over strong consistency.

*************************

- Question 5:

The CAP theorem guides microservices to balance consistency, availability, and partition tolerance. Architectures often prioritize partition tolerance and availability for robust and resilient systems.

*************************

- Question 6:

In the cloud, microservices enable scalability by allowing independent scaling of services based on demand. Cloud platforms facilitate automatic horizontal scaling for efficient resource utilization.

*************************

- Question 7:

Statelessness in microservices means each service operates independently without relying on the state of others. It simplifies scaling, independent deployment, and improves resiliencee

*************************

- Question 8:

An API Gateway acts as a central entry point for managing API calls in microservices. It handles tasks like authentication, authorization, load balancing, and request/response transformation. The API Gateway simplifies client interactions and enforces policies across the microservices ecosystemm

